package com.przemek.Note.Taking.controller;

import com.przemek.Note.Taking.model.Note;
import com.przemek.Note.Taking.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NoteController implements Serializable {

    @Autowired
    private NoteRepository noteRepository;

    @GetMapping("/notes")
    private List<Note> showAll(){
        Sort sortById = new Sort(Sort.DEFAULT_DIRECTION.DESC, "id");
        return noteRepository.findAll(sortById);
    }

    @PostMapping("/notes")
    private Note createNote(@Valid @RequestBody Note note){
        return noteRepository.save(note);
    }

    @GetMapping("/notes/{id}")
    private ResponseEntity<Note> getById(@PathVariable(value = "id") Long noteId){
        Note note = noteRepository.findOne(noteId);
        if (note == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(note);
    }

    @PutMapping("/notes/{id}")
    private ResponseEntity<Note> updateById(@PathVariable(value = "id") Long noteId,
                                            @Valid @RequestBody Note noteDetails){
        Note note = noteRepository.findOne(noteId);
        if (note == null) {
            return ResponseEntity.notFound().build();
        }

        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());

        Note updateNote = noteRepository.save(note);
        return ResponseEntity.ok(updateNote);
    }

    @DeleteMapping("/notes/{id}")
    private ResponseEntity<Note> deleteById(@PathVariable(value = "id") Long noteId) {
        Note note = noteRepository.findOne(noteId);
        if (note == null) {
            return ResponseEntity.notFound().build();
        }
        noteRepository.delete(note);
        return ResponseEntity.ok().build();
    }

}
